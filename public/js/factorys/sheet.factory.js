
'use strict';

module.exports =
  angular
  .module('diDocuments.sheet', [])
  .factory('Sheet', function() {

  return function(sheetData) {

    angular.extend(this, {
      id: new Date().getTime(),
      title: '乐游博客-阿明的博客-站内提供资源分享',
      body: require('raw!../../../README.md')
    });

    return angular.extend(this, sheetData);
  };

});
