'use strict';

var express = require('express')
  , app = module.exports = express()
  , fs = require('fs')
  , path = require('path')
  , md = require('./markdown-it.js').md
  , temp = require('temp')
  , phantom = require('phantom')
  , breakdance = require('breakdance')
  ;

const phantomSession = phantom.create()

function getPhantomSession() { return phantomSession }

function _getFullHtml(name, str, style, isLocal) {
  if (isLocal) {
    return '<div id="preview">\n'
    +'<style>'+ ((style) ? style : '') + '</style>'
    + md.render(str) + '\n</div>';
  }
    return '<!DOCTYPE html><html><head><meta charset="utf-8">'
    +'<meta name="keywords" content="个人博客,阿明的个人博客,个人博客模板,阿明,免费教程，源码分享" />'
    +'<meta name="description" content="阿明的个人博客,是一个会写代码的美工,网站提供免费的教学资源以及网站源码分享，欢迎各位小伙伴来网站留言互动。" />'
    +'<script>var _hmt = _hmt || [];(function() {var hm = document.createElement("script");'
		+'hm.src = "https://hm.baidu.com/hm.js?b6693eacab15fc17ab94ccbfecadfa2e";var s = document.getElementsByTagName("script")[0];'
    +'s.parentNode.insertBefore(hm, s);})();</script>'
    +'<link rel="shortcut icon" sizes="48x48" href="https://www.soscoon.com/public/logo-48.ico">'
    + '<title>'+ name + '</title><style>'
    + ((style) ? style : '') + '</style></head><body id="preview">\n'
    + md.render(str) + '\n</body></html>';
}

function _getHtml(str) { return md.render(str) }

// Move this into _getFormat() to reload the CSS without restarting node.
var _format = fs.readFileSync(path.resolve(__dirname, '../../public/css/app.css')).toString('utf-8');

function _getFormat() {
  return _format;
}

var fetchMd = function (req, res) {
  var unmd = req.body.unmd
    , json_response =
      {
        data: ''
        , error: false
      }

  var name = req.body.name.trim()

  if (!name.includes('.md')) {
    name = name + '.md'
  }

  if (req.body.preview === 'false') {
    res.attachment(name);
  } else {
    // We don't use text/markdown because my "favorite" browser
    // (IE) ignores the Content-Disposition: inline; and prompts
    // the user to download the file.
    res.type('text');

    // For some reason IE and Chrome ignore the filename
    // field when Content-Type: text/plain;
    res.set('Content-Disposition', `inline; filename="${name}"`);
  }

  res.end(unmd);
}

var fetchHtml = function (req, res) {
  var unmd = req.body.unmd
    , json_response =
      {
        data: ''
        , error: false
      }

  // For formatted HTML or not...
  var format = req.body.formatting ? _getFormat() : "";

  var html = _getFullHtml(req.body.name, unmd, format, req.query.savaLocal);

  var name = req.body.name.trim() + '.html'

  var filename = path.resolve(__dirname, '../../downloads/files/html/' + name)

  // 将PDF保存到本地
  if (req.query.savaLocal) {
    // 写入文件到本地硬盘
    fs.writeFile(
      path.join('/usr/site/front-end-server/viewSource/', `${name}`), html,
      (err) => {
        if (!err) {
          console.log(" write html to file ok");
          res.send(200, { data: { message: '保存成功', fileName: `${name}` } });
        } else {
          res.send('出错了', err);
        }
      }
    );
  } else {
    if (req.body.preview === 'false') {
      res.attachment(name);
    } else {
      res.type('html');
      res.set('Content-Disposition', `inline; filename="${name}"`);
    }
    res.end(html);
  }
}

var fetchPdf = function (req, res) {
  var unmd = req.body.unmd
    , json_response =
      {
        data: ''
        , error: false
      }

  var html = _getFullHtml(req.body.name, unmd, _getFormat())
  var tempPath = temp.path({ suffix: '.htm' })
  fs.writeFile(tempPath, html, 'utf8', function fetchPdfWriteFileCb(err, data) {
    if (err) {
      console.error(err);
      res.end("Something wrong with the pdf conversion.");
    } else {
      _createPdf(req, res, tempPath);
    }
  });
}

function _createPdf(req, res, tempFilename) {
  getPhantomSession().then(phantom => {
    return phantom.createPage();
  }).then(page => {
    page.open(tempFilename).then(status => {
      _renderPage(page);
    });
  });

  function _renderPage(page) {
    var name = req.body.name.trim() + '.pdf'
    var filename = temp.path({ suffix: '.pdf' })

    page.property('paperSize', { format: 'A4', orientation: 'portrait', margin: '1cm' })
    page.property('viewportSize', { width: 1024, height: 768 })

    page.render(filename).then(function () {
      if (req.body.preview === 'false') {
        res.attachment(name)
      } else {
        res.type('pdf')
        res.set('Content-Disposition', `inline; filename="${name}"`)
      }

      res.sendFile(filename, {}, function () {
        // Cleanup.
        fs.unlink(filename)
        fs.unlink(tempFilename)
      });

      page.close()
    });
  }
}

// Convert HTML to MD
function htmlToMd(req, res) {
  var md = ''

  try {
    md = breakdance(req.body.html)
  } catch (e) {
    return res.status(400).json({ error: { message: 'Something went wrong with the HTML to Markdown conversion.' } })
  }

  return res.status(200).json({ convertedMd: md })

}


/* Start Dillinger Routes */

// Download a markdown file directly as response.
app.post('/factory/fetch_markdown', fetchMd)

// Download an html file directly as response.
app.post('/factory/fetch_html', fetchHtml)

// Download a pdf file directly as response.
app.post('/factory/fetch_pdf', fetchPdf)

// Download a pdf file directly as response.
app.post('/factory/html_to_md', htmlToMd)

/* End Dillinger Core */
